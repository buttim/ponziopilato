#include <toneAC.h>
#include <LowPower.h>
#include <avr/pgmspace.h> 

//http://www.arduino.cc/en/Tutorial/Tone
//created by by Tom Igoe
#include "pitches.h"

#define REST  0

#include "feroVecio.h"
#include "godfather.h"
#include "princeIgor.h"
#include "odeToJoy.h"
#include "happyBirthday.h"
#include "starwars.h"
#include "furElise.h"
#include "elephantWalk.h"
#include "pinkPanther.h"
#include "brahmsLullaby.h"
#include "takeOnMe.h"
#include "monferrina.h"

int *songs[]={
  monferrina,
  feroVecio,
  //following melodies by Robson Couto (https://github.com/robsoncouto/arduino-songs)
  godfather,
  princeIgor,
  odeToJoy,
  happyBirthday,
  starWars,
  furElise,
  elephantWalk,
  pinkPanther,
  brahmsLullaby,
  takeOnMe
};

int *melody=songs[0],
  tempo=1000,
  nNote=0;
bool playing=false;
unsigned long t0,tNext=0;

//From https://provideyourown.com/2012/secret-arduino-voltmeter-measure-battery-voltage/
//Secret Arduino Voltmeter – Measure Battery Voltage by Provide Your Own is licensed 
//under a Creative Commons Attribution-ShareAlike 4.0 International License.
long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
     ADMUX = _BV(MUX5) | _BV(MUX0) ;
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  
 
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring
 
  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both
 
  long result = (high<<8) | low;
 
  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

int calcTempo(int *melody) {
  int n=0, tot=0, d;

  while (true) {
    int t=pgm_read_word_near(melody+2*n++ +1);
    if (t==0) break;
    d=1000/abs(t);
    if (t<0) d+=d/2;
    tot+=d+50;
  };
  return 1000*(19000.0 /tot);
}

void setup() {
  Serial.begin(115200);
  pinMode(LED_BUILTIN,OUTPUT);
  Serial.print("VCC: ");
  Serial.println(readVcc());

  randomSeed(analogRead(A0));
  int n=random(sizeof songs/sizeof(*songs));
  
  melody=songs[n];
  tempo=calcTempo(melody);
  Serial.print("tempo:");
  Serial.println(tempo);
  
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN,LOW);
  if (readVcc()<2750) while(true);

  t0=millis();
  pinMode(2,OUTPUT);
  pinMode(A1,OUTPUT);
  pinMode(A2,OUTPUT);
  pinMode(A3,OUTPUT);
  pinMode(A4,OUTPUT);
  digitalWrite(2,LOW);
  digitalWrite(A1,HIGH);
  digitalWrite(A3,HIGH);
  for (int i=0;i<5;i++) {
    digitalWrite(LED_BUILTIN,HIGH);
    delay(100);
    digitalWrite(LED_BUILTIN,LOW);
    delay(100);
  }
}

void ledOn() {
  digitalWrite(A2,LOW);
  digitalWrite(A4,LOW);
  digitalWrite(LED_BUILTIN,HIGH);
}

void ledOff() {
  digitalWrite(A2,HIGH);
  digitalWrite(A4,HIGH);
  digitalWrite(LED_BUILTIN,LOW);
}

void stop() {
    noToneAC();
    digitalWrite(A2,HIGH);
    digitalWrite(A4,HIGH);
    digitalWrite(LED_BUILTIN,LOW);
    LowPower.powerDown(SLEEP_FOREVER,ADC_OFF,BOD_OFF);
}

void loop() {
  unsigned long tNow=millis(),t=tNow-t0;
  
  if (t<5000) 
    t%=1000;
  else if (t<10000)
    t%=500;
  else if (t<15000)
    t%=250;
  else if (t<20000)
    t%=100;
  else 
    stop();
    
  if (t<50) ledOn(); else ledOff();

  //music
  if (tNow>=tNext) 
    if (playing) {
      noToneAC();
      playing=false;
      tNext=tNow+tempo/20;
    }
    else {
      int n=pgm_read_word_near(melody+nNote*2+1);
      if (n==0) stop();
      
      int noteDuration=tempo/abs(n);
      if (n<0)
        noteDuration+=noteDuration/2;
      toneAC(pgm_read_word_near(melody+nNote++*2));
      tNext=tNow+noteDuration;
      playing=true;
    }
}
